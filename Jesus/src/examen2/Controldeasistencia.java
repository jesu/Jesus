package examen2;

import java.sql.Date;


public class Controldeasistencia {
    
	private int codigo;
	private String nombre ;
	private String ApeMaterno;
	private String ApePaterno;
	private Date Fecha;
	private String dni;
	private String celular;
	private String email;
	private String direccion;
	private String religion;
	private double sexo ;
	private String estadoCivil1;
	private string HoradeEntrada;
	private string HoradeSalida;
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApeMaterno() {
		return ApeMaterno;
	}
	public void setApeMaterno(String apeMaterno) {
		ApeMaterno = apeMaterno;
	}
	public String getApePaterno() {
		return ApePaterno;
	}
	public void setApePaterno(String apePaterno) {
		ApePaterno = apePaterno;
	}
	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public double getSexo() {
		return sexo;
	}
	public void setSexo(double sexo) {
		this.sexo = sexo;
	}
	public String getEstadoCivil1() {
		return estadoCivil1;
	}
	public void setEstadoCivil1(String estadoCivil1) {
		this.estadoCivil1 = estadoCivil1;
	}
	public string getHoradeEntrada() {
		return HoradeEntrada;
	}
	public void setHoradeEntrada(string horadeEntrada) {
		HoradeEntrada = horadeEntrada;
	}
	public string getHoradeSalida() {
		return HoradeSalida;
	}
	public void setHoradeSalida(string horadeSalida) {
		HoradeSalida = horadeSalida;
	}

	
	
	
	
}

